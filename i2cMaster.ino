#include <Wire.h>
int counter = 0;



void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(115200);  // start serial for output

  //delay(5000);
  DetectDevices();
}

void loop() {
  DetectDevices();
 //RequestState(2);
//  RequestState(55);
//  Serial.println();
  delay(500);
}

void DetectDevices() {
  Serial.println("Detecting i2c devices");
  for (int i = 0; i < 127; i++) {
    Wire.beginTransmission(i);
    int error = Wire.endTransmission();

    if (error == 0)
    {
      Serial.print("I2C device found at address ");

      Serial.println(i);
    }
    else if (error == 4)
    {
      Serial.print("Unknown error at address ");
      Serial.print(i);
    }
    delay(50);
  }

  
    Serial.println("done\n");
  
}

void SendData(int address, char data) {
  Wire.beginTransmission(address);
  Wire.write(data);
  Wire.endTransmission();
}

void RequestState(int address) {
  Wire.requestFrom(address, 16);    // request 6 bytes from slave device #8
  counter = 0;
  while (Wire.available()) { // slave may send less than requested
    int c = Wire.read(); // receive a byte as character
    Serial.print(c);         // print the character
    Serial.print(' ');
    counter++;
  }
  Serial.println();
  Serial.print(counter);
  Serial.println(" bytes received");
}
